var btdotjsProfile = angular.module('btdotjsProfile', []);
btdotjsProfile.controller('btdotjsCtl', function($scope, $http) {
  $scope.localization = {};
  $scope.localization.mynameis = "Barry Teoh";
  $scope.localization.lastseen = "Last seen:";
  if (navigator.language.indexOf("ja") >= 0) {
    $scope.localization.mynameis = "張バリー";
    $scope.localization.lastseen = "最後の場所:";
  } else if (navigator.language.indexOf("zh") >= 0) {
    $scope.localization.mynameis = "張振榮";
    $scope.localization.lastseen = "在:";
  } else if (navigator.language.indexOf("ko") >= 0) {
    $scope.localization.mynameis = "장진영";
    $scope.localization.lastseen = "행방:";
  } else if (navigator.language.indexOf("ru") >= 0) {
    $scope.localization.mynameis = "Барри Чжан";
    $scope.localization.lastseen = "Последний видеться:";
  }
  var numZ = "0";
  var num1 = "1";
  var num2 = "2";
  var num3 = "3";
  var num4 = "4";
  var num5 = "5";
  var num6 = "6";
  var num7 = "7";
  var num8 = "8";
  var num9 = "9";

  $scope.contact = {
    "e1": "hel",
    "e2": "lo@",
    "h1": "barr",
    "h2": "yteo",
    "h3": "h.c",
    "h4": "om"
  }
  $scope.where = {
    "name": "Loading...",
    "code": "XX",
    "friendlyname": "Loading...",
    "phnum": "Loading...",
    "lastseen": "",
    geo: {
      "lat": 0,
      "lng": 0
    },
    "profilephotourl": "//barryteoh.com/images/avatar_OTHER_1_122x122.jpg"
  }
  $http.get('https://otgqnsn74l.execute-api.us-east-1.amazonaws.com/prod/Locator').success(function(data) {
    if (data.meta.status == 200) {
      $scope.where = {
        "name": data.where.name,
        "code": data.where.code,
        "friendlyname": data.where.name,
        "geo": data.where.geo
      }
      switch (data.where.code) {
        case "HK":
          $scope.where.profilephotourl = "//barryteoh.com/images/avatar_HKG_1_122x122.jpg";
          $scope.where.phnum = "+" + num8 + num5 + num2 + "-" + num6 + num8 + num4 + num7 + "-" + num5 + num7 + num2 + numZ;
          break;
        case "KH":
          $scope.where.profilephotourl = "//barryteoh.com/images/avatar_KH_1.png";
          $scope.where.phnum = "No number available";
          break;
        case "MM":
          $scope.where.profilephotourl = "//barryteoh.com/images/avatar_MM_1_122x122.jpg";
          $scope.where.phnum = "No number available";
          break;
        case "TH":
          $scope.where.profilephotourl = "//barryteoh.com/images/avatar_MM_1_122x122.jpg";
          $scope.where.phnum = "No number available";
          break;
        case "MY":
          $scope.where.profilephotourl = "//barryteoh.com/images/avatar_MM_1_122x122.jpg";
          $scope.where.phnum = "No number available";
          break;
        case "LA":
          $scope.where.profilephotourl = "//barryteoh.com/images/avatar_MM_1_122x122.jpg";
          $scope.where.phnum = "No number available";
          break;
        case "AU":
          var diceRoll1 = Math.floor((Math.random() * 100) + 1);
          var diceRoll2 = Math.floor((Math.random() * 100) + 1);
          // is it even or is it odd?
          if (((diceRoll1 + diceRoll2) % 2) == 0) {
            $scope.where.profilephotourl = "//barryteoh.com/images/avatar_SYD_2_122x122.jpg";
          } else {
            $scope.where.profilephotourl = "//barryteoh.com/images/avatar_SYD_1_122x122.jpg";
          }
          $scope.where.phnum = "+" + num6 + num1 + "-" + num4 + numZ + num8 + "-" + num6 + num5 + num4 + "-" + num9 + num7 + num1;
          break;
        case "CN":
          $scope.where.profilephotourl = "//barryteoh.com/images/avatar_CN_1_122x122.jpg";
          // $scope.where.phnum = "+" + num8 + num6 + "-" + num1 + num3 + num1 + "-" + num4 + num7 + num5 + num1 + "-" + numZ + num5 + num7 + num4;
          $scope.where.phnum = "+" + num8 + num5 + num2 + "-" + num6 + num8 + num4 + num7 + "-" + num5 + num7 + num2 + numZ;
          break;
        case "JP":
          $scope.where.profilephotourl = "//barryteoh.com/images/avatar_JP_1_122x122.jpg";
          $scope.where.phnum = "Currently in Transit"; // No Number available
          break;
        case "TW":
          $scope.where.profilephotourl = "//barryteoh.com/images/avatar_TW_1_122x122.jpg";
          $scope.where.phnum = "+" + num8 + num8 + num6 + "-" + num9 + numZ + num9 + "-" + num7 + numZ + num6 + "-" + num2 + num5 + num3;
          break;
        case "US":
          // Show city specific photos? Lets see
          if (data.where.city == "San Francisco") {
            var diceRoll1 = Math.floor((Math.random() * 100) + 1);
            var diceRoll2 = Math.floor((Math.random() * 100) + 1);
            if (((diceRoll1 + diceRoll2) % 2) == 0) {
              $scope.where.profilephotourl = "//barryteoh.com/images/avatar_SF_2_122x122.png"; // San Francisco 2
            } else {
              $scope.where.profilephotourl = "//barryteoh.com/images/avatar_SF_1_122x122.png"; // San Francisco 1
            }
          } else {
            $scope.where.profilephotourl = "//barryteoh.com/images/avatar_SF_2_122x122.png"; // San Francisco as default
          }

          $scope.where.phnum = "+" + num1 + "-" + num4 + num7 + num8 + "-" + num2 + num2 + num7 + "-" + num7 + num9 + num1 + num1
          break;
        case "CA":
          // Show default profile
          var diceRoll1 = Math.floor((Math.random() * 100) + 1);
          var diceRoll2 = Math.floor((Math.random() * 100) + 1);
          if (((diceRoll1 + diceRoll2) % 2) == 0) {
            $scope.where.profilephotourl = "//barryteoh.com/images/avatar_OTHER_2_122x122.png";
          } else {
            $scope.where.profilephotourl = "//barryteoh.com/images/avatar_VANCITY_1_122x122.png";
          }
          // Use USA Number
          $scope.where.phnum = "+" + num1 + "-" + num4 + num7 + num8 + "-" + num2 + num2 + num7 + "-" + num7 + num9 + num1 + num1
          break;
        case "KR":
          var diceRoll1 = Math.floor((Math.random() * 100) + 1);
          var diceRoll2 = Math.floor((Math.random() * 100) + 1);
          var diceRoll3 = Math.floor((Math.random() * 100) + 1);
          var diceRoll4 = Math.floor((Math.random() * 100) + 1);
          if (((diceRoll1 + diceRoll2) % 2) == 0) {
            if (((diceRoll3 + diceRoll4) % 2) != 0) {
              $scope.where.profilephotourl = "//barryteoh.com/images/avatar_KOR_3_122x122.jpg";
            } else {
              $scope.where.profilephotourl = "//barryteoh.com/images/avatar_KOR_1_122x122.jpg";
            }
          } else {
            if (((diceRoll3 + diceRoll4) % 2) == 0) {
              $scope.where.profilephotourl = "//barryteoh.com/images/avatar_KOR_3_122x122.jpg";
            } else {
              $scope.where.profilephotourl = "//barryteoh.com/images/avatar_KOR_2_122x122.jpg";
            }
          }
          $scope.where.phnum = "+" + num8 + num2 + "-" + num1 + numZ + "-" + num6 + num5 + num2 + num4 + "-" + num1 + num4 + num2 + num5;
          break;
        default:
          // Travelling (Unknown country)
          var diceRoll1 = Math.floor((Math.random() * 100) + 1);
          var diceRoll2 = Math.floor((Math.random() * 100) + 1);
          if (((diceRoll1 + diceRoll2) % 2) == 0) {
            $scope.where.profilephotourl = "//barryteoh.com/images/avatar_OTHER_1_122x122.jpg";
          } else {
            $scope.where.profilephotourl = "//barryteoh.com/images/avatar_OTHER_2_122x122.png";
          }
          $scope.where.phnum = "No phone number available"; // No Number available
      }
      if (data.where.city != undefined) {
        $scope.where.city = data.where.city;
        $scope.where.friendlyname = data.where.city + ", " + data.where.name;
      }
      if (data.where['lastseen_timestamp'] != undefined) {
        var currentTime =  Math.floor(new Date().getTime() / 1000);
        var TimeDiff = currentTime - parseInt(data.where['lastseen_timestamp']);
        if (TimeDiff < 259200) { // Less than 3 days
          $scope.where.lastseen = moment.unix(parseInt(data.where['lastseen_timestamp'])).fromNow(); // Uses momentjs
        } else {
          $scope.where.lastseen = ''; // Don't show it
        }
      } else {
        $scope.where.lastseen = '';
      }
    } else {
      // Non-200
      $scope.where = {
        "name": "Unknown",
        "code": "XX",
        "friendlyname": "Unknown (Error with location service)",
        "phnum": "Unknown (Error with location service)",
        "lastseen": "",
        "profilephotourl": "//d3hs7z89jfjpsh.cloudfront.net/photos/avatar_OTHER_1_122x122.jpg"
      }
    }
  });
});
